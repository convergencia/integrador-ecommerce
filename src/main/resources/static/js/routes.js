'use strict';

/**
 * Route configuration for the RDash module.
 */
angular.module('CIntegrator').config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
    function ($stateProvider, $urlRouterProvider, $httpProvider) {

        // For unmatched routes
        $urlRouterProvider.otherwise('/');

        // Application routes
        $stateProvider
                .state('index', {
                    data: {pageTitle: 'Dashboard', pageDetail: 'Monitor de andamento', breadcrumb: 'Home / Dashboard'},
                    url: '/',
                    templateUrl: 'templates/dashboard.html'
                })
                .state('loja', {
                    data: {pageTitle: 'Loja', pageDetail: 'Cadastro e manutenção', breadcrumb: 'Home / Loja'},
                    url: '/loja',
                    templateUrl: 'templates/cadastro-loja.html'
                })
                .state('produto', {
                    data: {pageTitle: 'Produto', pageDetail: 'Cadastro e manutenção', breadcrumb: 'Home / Produto'},
                    url: '/produto',
                    templateUrl: 'templates/cadastro-produto.html'
                })
                .state('departamento', {
                    data: {pageTitle: 'Departamento', pageDetail: 'Cadastro e manutenção', breadcrumb: 'Home / Departamento'},
                    url: '/departamento',
                    templateUrl: 'templates/cadastro-departamento.html'
                })
                .state('categoria', {
                    data: {pageTitle: 'Categoria', pageDetail: 'Cadastro e manutenção', breadcrumb: 'Home / Categoria'},
                    url: '/categoria',
                    templateUrl: 'templates/cadastro-categoria.html'
                })
                .state('sub-categoria', {
                    data: {pageTitle: 'Sub-categoria', pageDetail: 'Cadastro e manutenção', breadcrumb: 'Home / Sub-categoria'},
                    url: '/sub-categoria',
                    templateUrl: 'templates/cadastro-subcategoria.html'
                })
                .state('cliente', {
                    data: {pageTitle: 'Cliente', pageDetail: 'Cadastro e manutenção', breadcrumb: 'Home / Cliente'},
                    url: '/cliente',
                    templateUrl: 'templates/cadastro-cliente.html'
                })
                .state('estoque', {
                    data: {pageTitle: 'Estoque', pageDetail: 'Cadastro e manutenção', breadcrumb: 'Home / Estoque'},
                    url: '/estoque',
                    templateUrl: 'templates/cadastro-estoque.html'
                })
                .state('precificacao', {
                    data: {pageTitle: 'Precificação', pageDetail: 'Cadastro e manutenção', breadcrumb: 'Home / Precificação'},
                    url: '/precificacao',
                    templateUrl: 'templates/precificacao.html'
                })
                .state('pedido', {
                    data: {pageTitle: 'Pedido', pageDetail: 'Cadastro e manutenção', breadcrumb: 'Home / Pedido'},
                    url: '/pedido',
                    templateUrl: 'templates/pedido.html'
                })
                .state('pedido-detalhe', {
                    data: {pageTitle: 'Pedido detalhe', pageDetail: 'Cadastro e manutenção', breadcrumb: 'Home / Pedido detalhe'},
                    url: '/pedido/detalhe/:id',
                    templateUrl: 'templates/pedido-detalhe.html'
                })
                .state('pedido-lista', {
                    data: {pageTitle: 'Pedido detalhe', pageDetail: 'Cadastro e manutenção', breadcrumb: 'Home / Pedido detalhe'},
                    url: '/pedido/lista/:status',
                    templateUrl: 'templates/lista-pedido.html'
                })
                .state('job-log', {
                    data: {pageTitle: 'Job execuções', pageDetail: 'Detalhe', breadcrumb: 'Home / Job detalhe'},
                    url: '/jobs/detalhe/:id',
                    templateUrl: 'templates/job-detalhe.html'
                })
                .state('categorizacao', {
                    data: {pageTitle: 'Categorização', pageDetail: 'Cadastro e manutenção', breadcrumb: 'Home / Categoriazação'},
                    url: '/categorizacao',
                    templateUrl: 'templates/categorizacao.html'
                })
                .state('jobs', {
                    data: {pageTitle: 'Jobs', pageDetail: 'Manutenção dos processo de integrações', breadcrumb: 'Home / Jobs'},
                    url: '/jobs',
                    templateUrl: 'templates/job.html'
                })
                .state('erro-web-venda', {
                    data: {pageTitle: 'Notificação', pageDetail: 'Notificações recebida da WebVendas', breadcrumb: 'Home / Notificação WebVendas'},
                    url: '/erro-web-venda',
                    templateUrl: 'templates/erro-web-venda.html'
                })
                .state('login', {
                    url: '/login',
                    data: {pageTitle: 'Login', pageDetail: 'Cadastro e manutenção', breadcrumb: 'login'},
                    templateUrl: 'templates/login.html'
                });
        $httpProvider.interceptors.push('SecurityInterceptor');
    }
]);

angular.module('CIntegrator').run(['$rootScope', '$state', '$stateParams',
    function ($rootScope, $state, $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    }]);