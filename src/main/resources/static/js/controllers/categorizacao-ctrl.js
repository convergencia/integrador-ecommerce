/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('CIntegrator').controller('CategorizacaoCtrl', ['$scope', 'CategoriaService', 'ProdutoService', CategorizacaoCtrl]);

function CategorizacaoCtrl($scope, CategoriaService, ProdutoService) {
    var self = this;
    self.paramConsulta = {};
    self.itemSelecionado = new CategoriaService();
    self.departamentos = [];
    self.categorias = [];
    self.subCategorias = [];
    self.subCategoria = new CategoriaService();

    self.produtos = [];
    self.produtosSelecionado = [];
    self.produtoService = new ProdutoService();
    self.paramConsulta = {};

    self.initSubCategorias = function () {
        CategoriaService.departamentos(function (response) {
            self.departamentos = response;
        });

    };

    self.selectCategoriaByDept = function () {
        CategoriaService.categoriaBy(self.departamento, function (response) {
            delete self.categorias;
            self.categorias = response;
        });
    };

    self.selectSubCategoriaBy = function () {
        CategoriaService.categoriaBy(self.itemSelecionado.categoriaPai, function (response) {
            delete self.subCategorias;
            self.subCategorias = response;
        });
    };


    self.delete = function (item) {
        CategoriaService.delete({id: item.id}, function () {
            self.initSubCategorias();
        });
    };

    self.askDelete = function (item) {
        if (window.confirm("Deseja relamente excluir item ?")) {
            self.delete(item);
        }

    };

    self.select = function (it) {
        self.itemSelecionado = it;
        self.departamento = it.categoriaPai.categoriaPai;
        self.selectCategoriaByDept();
    };

    self.selecionados = function (it) {
        self.produtosSelecionado.push(it);
        self.produtosSelecionado = self.produtosSelecionado.filter(function (item) {
            if (item.selecionado) {
                return item;
            }
        });
    };

    self.searchProduto = function () {
        ProdutoService.search(self.paramConsulta, function (response) {
            self.produtos = response;
        }, function (err) {
            console.info(err);
        });
    };

    self.saveProduto = function () {
        if (self.produtosSelecionado.length > 0) {
            var item = self.produtosSelecionado.pop();
            self.produtoService = item;
            self.produtoService.categoria = self.subCategoria;
            ProdutoService.save(self.produtoService, function (response) {
                self.produtoService = new ProdutoService();
                self.saveProduto();
            });

        }
        self.searchProduto();
    };
    
    self.avancar = function (){ 
        $scope.activeTabCategorizacao = 1;
    };
    
    self.clear = function (){
        self.produtosSelecionado = []; 
        $scope.activeTabCategorizacao = 0;
    };
    
}



