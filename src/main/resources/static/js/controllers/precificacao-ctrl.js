/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('CIntegrator').controller('PrecificacaoCtrl', ['$scope', '$uibModal', 'EstoqueService', PrecificacaoCtrl]);

function PrecificacaoCtrl($scope, $uibModal, EstoqueService) {
    var self = this;


    self.itens = [];

    self.openPainelConsulta = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'myModalContent.html',
            controller: function ($uibModalInstance, ProdutoService) {
                var $ctrl = this;
                $ctrl.items = [];
                $ctrl.paramConsulta = {};

                $ctrl.pesquisar = function () {
                    if ($ctrl.paramConsulta !== '' && $ctrl.paramConsulta.desc !== undefined && $ctrl.paramConsulta.desc.length > 2) {
                        ProdutoService.search($ctrl.paramConsulta, function (response) {
                            response.forEach(function (it) {
                                $ctrl.items.push({seleced: false, itm: it});
                            });

                        }, function (err) {
                            console.info(err);
                        });
                    }

                };

                $ctrl.checkAll = function () {
                    if ($ctrl.selectedAll) {
                        $ctrl.selectedAll = true;
                    } else {
                        $ctrl.selectedAll = false;
                    }
                    angular.forEach($ctrl.items, function (item) {
                        item.selected = $ctrl.selectedAll;
                    });

                };

                $ctrl.ok = function () {

                    var itns = [];
                    $ctrl.items.forEach(function (it) {
                        if (it.selected) {
                            itns.push(it.itm);
                        }
                    });



                    $uibModalInstance.close(itns);
                };

                $ctrl.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            },
            controllerAs: '$ctrl',
            size: 'lg'
        });

        modalInstance.result.then(function (selectedItem) {

            selectedItem.forEach(function (i) {
                EstoqueService.getEstoqueProduto({}, i, function (result) {

                    if (self.itens.every(function (i) {
                        return i.it.id !== result.id;
                    })) {
                        self.itens.push({precoVenda: result.precoVenda, precoOferta: result.precoOferta, it: result});
                    }


                });
            });

        }, function () {
            console.info('Modal dismissed at: ' + new Date());
        });
    };

    self.askDelete = function (item) {
        if (window.confirm("Deseja ralmente excluir o item ?")) {
            var index = self.itens.indexOf(item);
            self.itens.splice(index, 1);
        }
    };

    self.salvarAlteracaoPreco = function () {

        var item = self.itens.shift();
        if (item !== undefined) {
            do {

                item.it.precoOferta = item.precoOferta;
                item.it.precoVenda = item.precoVenda;
                item.it.statusIntegracaoPreco = 'NAO_INTEGRADO';

                EstoqueService.save(item.it);

                item = self.itens.shift();
            } while (item !== undefined)
        }


    };
}