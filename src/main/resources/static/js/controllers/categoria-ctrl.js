/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('CIntegrator').controller('CategoriaCtrl', ['$scope', 'CategoriaService', CategoriaCtrl]);

function CategoriaCtrl($scope, CategoriaService) {
    var self = this;
    self.paramConsulta = {};
    self.itemSelecionado = new CategoriaService();
    self.departamentos = [];
    self.itens = [];



    self.initCategorias = function () {
        CategoriaService.departamentos(function (response) {
            self.departamentos = response;
        });

        
        CategoriaService.categorias(function (response) {
            self.itens = response;
        });
    };


    self.save = function () {
        self.itemSelecionado.$save(function (response) {
            self.initCategorias();
            self.itemSelecionado = new CategoriaService();
        });
    };

    self.searchCategoria = function () {
        self.paramConsulta.tipo = "categoria";
        CategoriaService.search(self.paramConsulta, function (response) {
            self.itens = response;
        }, function (err) {
            console.info(err);
        });
    };

    self.delete = function (item) {
        CategoriaService.delete({id: item.id}, function () {
            self.initCategorias();
        });
    };

    self.askDelete = function (item) {
        if (window.confirm("Deseja relamente excluir item ?")) {
            self.delete(item);
        }

    };

    self.select = function (it) {
        self.itemSelecionado = it;
        $scope.activeTabCategoria = 1;
    };
    
    self.clear = function (){
        self.itemSelecionado = new CategoriaService();
        $scope.activeTabCategoria = 0;
    }


}