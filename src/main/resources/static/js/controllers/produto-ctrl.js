/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('CIntegrator').controller('ProdutoCtrl', ['$scope', 'ProdutoService', ProdutoCtrl]);

function ProdutoCtrl($scope, ProdutoService) {
    var self = this;
    self.itemSelecionado = new ProdutoService();
    self.paramConsulta = {};
    self.itens = [];

    self.init = function () {
//		ProdutoService.query(function(response) {
//			self.itens = response;
//		});
    };
 
    self.searchProduto = function () {
        ProdutoService.search(self.paramConsulta, function (response) {
            self.itens = response;
        }, function (err) {
            console.info(err);
        });
    };

    self.save = function () {
        self.itemSelecionado.statusIntegracao = "NAO_INTEGRADO";
        self.itemSelecionado.$save(function (response) {
            self.itens.push(response);
            self.itemSelecionado = new ProdutoService();
        });
    };

    self.delete = function (item) {
        ProdutoService.delete({id: item.id}, function () {
            self.searchProduto();
        });
    };

    self.askDelete = function (item) {
        if (window.confirm("Deseja relamente excluir item ?")) {
            self.delete(item);
        }
        ;
    };

    self.select = function (it) {
        self.itemSelecionado = it;
        $scope.activeTabProduto = 1;
    };
    
    self.list = function () {
        ProdutoService.query(function(response) {
                self.itens = response;
        });
    };
    
    self.clear = function (){
        self.itemSelecionado = new ProdutoService();
        $scope.activeTabProduto = 0;
    }

    self.init();
}