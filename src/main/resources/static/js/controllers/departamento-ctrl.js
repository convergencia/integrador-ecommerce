/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('CIntegrator').controller('DepartamentoCtrl', ['$scope', 'CategoriaService', DepartamentoCtrl]);

function DepartamentoCtrl($scope, CategoriaService) {
    var self = this;
    self.paramConsulta = {};
    self.itemSelecionado = new CategoriaService();
    self.itens = [];

    self.initDepartamento = function () {
        CategoriaService.departamentos(function (response) {
            self.itens = response;
        });
    };

    self.save = function () {
        self.itemSelecionado.$save(function () {
            self.initDepartamento();
            self.itemSelecionado = new CategoriaService();
        });
    };

    self.searchDepartamento = function () {
        self.paramConsulta.tipo = "depertamento";
        CategoriaService.search(self.paramConsulta, function (response) {
            self.itens = response;
        }, function (err) {
            console.info(err);
        });
    };

    self.delete = function (item) {
        CategoriaService.delete({id: item.id}, function () {
            self.initDepartamento();
        });
    };

    self.askDelete = function (item) {
        if (window.confirm("Deseja relamente excluir item ?")) {
            self.delete(item);
        }

    };

    self.select = function (it) {
        self.itemSelecionado = it;
        $scope.activeTabDepartamento = 1;
    };
    
    self.clear = function (){
        self.itemSelecionado = new CategoriaService();
        $scope.activeTabDepartamento = 0;
    }


}