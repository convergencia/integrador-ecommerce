angular.module('CIntegrator').controller('DashBoardCtrl', ['$scope', '$stateParams', '$http', DashBoardCtrl]);

function DashBoardCtrl($scope, $stateParams, $http) {
    var self = this;
    self.itens = [];
    self.currentPage = 1;
    self.pedido = {};
    self.estoque = {};
    self.preco ={};
    self.loja ={};

    self.init = function () {

        $http.get('api/dashboard/info').then(function (restult) {
            self.pedido = restult.data.pedido;
            self.estoque = restult.data.estoque;
            self.preco = restult.data.preco;
            self.loja = restult.data.loja;

        });

    };

}