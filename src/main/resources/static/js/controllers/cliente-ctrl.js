/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('CIntegrator').controller('ClienteCtrl', ['$scope', 'ClienteService', 'SweetAlert', ClienteCtrl]);

function ClienteCtrl($scope, ClienteService, SweetAlert) {
    var self = this;
    self.itens = [];
    self.itemSelecionado = new ClienteService();
    self.currentPage = 1;


    self.paramDefault = {
        ufs: ['AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PR',
            'PB', 'PA', 'PE', 'PI', 'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SE', 'SP', 'TO']
    };

    self.init = function () {
        ClienteService.query(function (response) {
            self.itens = response
        });
    };

    self.save = function () {
        self.itemSelecionado.$save(function (response) {
            self.itens.push(response);
            SweetAlert.swal("Sucesso!", "Item " + self.itemSelecionado.nome + " adicionado", "success");
            self.itemSelecionado = new ClienteService();

        }, function () {
            SweetAlert.swal("Falha!", "Item " + self.itemSelecionado.nome + " com erro ao adicionar", "error");
        });
    };

    self.delete = function (item) {
        ClienteService.delete({id: item.id}, function () {
            self.init();
            SweetAlert.swal("Sucesso!", "Item " + item.id + " removido", "success");
        });
    };

    self.askDelete = function (item) {

        SweetAlert.swal({
                title: "Você tem certeza?",
                text: "Não será capaz de recuperar este Item!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, delete o item!",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    self.delete(item);
                }

            });
    };

    self.select = function (it) {
        self.itemSelecionado = it;
        $scope.activeTabClient = 1;
    };

    self.clear = function () {
        self.itemSelecionado = new ClienteService();
        $scope.activeTabClient = 0;
    }

    self.searchCliente = function () {
        self.itens = [];
        ClienteService.search(self.paramConsulta, function (response) {
            self.itens = response;
        }, function (err) {
            SweetAlert.swal("Falha!", "Erro ao consultar cliente \n" + err, "error");
        });
    }

}
