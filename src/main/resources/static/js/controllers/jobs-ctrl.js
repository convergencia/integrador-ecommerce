/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('CIntegrator').controller('JobCtrl', ['$scope', 'JobService', JobCtrl]);

function  JobCtrl($scope, JobService) {
    var self = this;
    self.itens = [];
    self.currentPage = 1;


    self.init = function () {
        JobService.query(function (response) {
            self.itens = response;
        });
    };

    self.ativaDesativa = function (item) {
        item.ativa = !item.ativa;
        item.$save(function () {
            self.init();
        });
    };

    self.processaManual = function (interface) {
        
        JobService.processarManual({"interface" : interface.nome}, function (response) {
            console.info(response);
        });
    };

}
