/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('CIntegrator').controller('PedidoProdutoCtrl', ['$scope', 'PedidoProdutoService', PedidoProdutoCtrl]);

function PedidoProdutoCtrl($scope, PedidoProdutoService){
    var self = this;
    self.itemSelecionado = new PedidoProdutoService();
    self.itens = [];
    self.paramDefault = {
        ufs: ['AC','AL','AP','AM','BA','CE','DF','ES','GO','MA','MT','MS','MG','PR',
            'PB','PA','PE','PI','RJ','RN','RS','RO','RR','SC','SE','SP','TO']
    }
    
    self.init = function(){
        PedidoProdutoService.query(function (response){
            self.itens = response;
        });
    };
    
    self.save = function(){
        self.itemSelecionado.$save(function (response){
            self.itens.push(response);
            self.itemSelecionado = new PedidoProdutoService();
        });
    };
    
    self.delete = function(item){
        PedidoProdutoService.delete({id: item.id}, function () {
           self.init(); 
        });
    };
    
    self.askDelete = function(item){
        if(window.confirm("Deseja relmente excluir item ?")){
           self.delete(item);
        }
    };
    
    self.select = function (it){
        self.itemSelecionado = it;
    };
    
    self.init();
}
