/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('CIntegrator').controller('PedidoRastreamentoCtrl', ['$scope', 'PedidoRastreamentoService', PedidoRastreamentoCtrl]);

function PedidoRastreamentoCtrl($scope, PedidoRastreamentoService){
    var self = this;
    self.itemSelecionado = new PedidoRastreamentoService();
    self.itens = [];
    self.paramDefault = {
        ufs: ['AC','AL','AP','AM','BA','CE','DF','ES','GO','MA','MT','MS','MG','PR',
            'PB','PA','PE','PI','RJ','RN','RS','RO','RR','SC','SE','SP','TO']
    }
    
    self.init = function(){
        PedidoRastreamentoService.query(function (response){
            self.itens = response;
        });
    };
    
    self.save = function(){
        self.itemSelecionado.$save(function (response){
            self.itens.push(response);
            self.itemSelecionado = new PedidoRastreamentoService();
        });
    };
    
    self.delete = function(item){
        PedidoRastreamentoService.delete({id: item.id}, function () {
           self.init(); 
        });
    };
    
    self.askDelete = function(item){
        if(window.confirm("Deseja relmente excluir item ?")){
           self.delete(item);
        }
    };
    
    self.select = function (it){
        self.itemSelecionado = it;
    };
    
    self.init();
}
