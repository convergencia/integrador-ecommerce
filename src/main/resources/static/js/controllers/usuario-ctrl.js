/* global CryptoJS */

angular.module('CIntegrator')
        .controller('UsuarioCtrl', ['$scope', 'UsuarioService'/*, '$uibModal', 'ModalInstanceCtrl'*/, UsuarioCtrl]);


function UsuarioCtrl($scope, UsuarioService/*, $uibModal, ModalInstanceCtrl*/) {

    var self = this;
    self.selected = new UsuarioService();
    self.usuarios = [];

    self.init = function () {
        UsuarioService.query(function (data) {
            self.usuarios = data;
        });
    };

    self.save = function () {
        self.selected.perfil = 1;
        self.selected.$save(function (data) {
            self.usuarios.push(data);
            self.selected = new UsuarioService();
        });
    };

    self.remove = function (item) {
        UsuarioService.delete({id: item.id}, function () {
            self.init();
        });

    };

    self.askDelete = function (item) {

        if (window.confirm("Deseja relamente excluir item ?")) {
            self.remove(item);
        }

//        var message = "Deseja relamente excluir item ?";
//
//        var modalHtml = '<div class="modal-body">' + message + '</div>';
//        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';
//
//        var modalInstance = $uibModal.open({
//            template: modalHtml,
//            controller: ModalInstanceCtrl
//        });
//
//        modalInstance.result.then(function () {
//            
//        });
    };

    self.select = function (it) {
        it.password = undefined;
        self.selected = it;
    };

    self.init();

}