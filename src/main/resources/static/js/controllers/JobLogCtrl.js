angular.module('CIntegrator').controller('JobLogCtrl', ['$scope', '$stateParams', 'JobLogService', JobLogCtrl]);

function JobLogCtrl($scope, $stateParams, JobLogService) {
    var self = this;
    self.itens = [];
    self.currentPage =1;
    self.job = $stateParams.id;

    self.init = function () {

        if ($stateParams.id !== undefined) {

            JobLogService.query({id: self.job}, function (response) {
                self.itens = response;
            });
        }

    };

}
