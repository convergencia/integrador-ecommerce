/* global CryptoJS */

angular.module('CIntegrator').service('UserService', ['$localStorage', UserService]);

function UserService($localStorage) {
    var service = this, currentUser = null;
    var storage = $localStorage;

    service.setCurrentUser = function (user) {
        currentUser = user;
        storage.user = user;
        return currentUser;
    };

    service.getCurrentUser = function () {
        if (!currentUser) {
            currentUser = storage.user;
        }
        return currentUser;
    };
}