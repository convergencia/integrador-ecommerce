angular.module('CIntegrator').factory('JobLogService', ['$resource', JobLogService]);

function JobLogService($resource){
    return $resource('api/job-log/:id', {
        query: '@id'
    });
}

