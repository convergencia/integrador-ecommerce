
angular.module('CIntegrator').factory('UsuarioService', ['$resource', UsuarioService]);

function UsuarioService($resource) {

    return $resource('api/usuario/:id', {
        query: '@id'
    });
}

