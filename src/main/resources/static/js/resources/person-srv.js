'use strict';

angular.module('CIntegrator').factory('PersonService', ['$http', '$q', 'Oauth', function ($http, $q, Oauth) {

        return {

            list: function () {
                return $http(Oauth.getRequest('/springboot-angular/sec/person', 'GET')).then(
                        function (response) {
                            return response.data;
                        },
                        function (errResponse) {
                            console.error('Error on list Person');
                            return $q.reject(errResponse);
                        }
                );
            },

            save: function (person) {
                return $http(Oauth.getRequest('/springboot-angular/sec/person', 'POST', person))
                        .then(
                                function (response) {
                                    return response.data;
                                },
                                function (errResponse) {
                                    console.error('Error on save Person');
                                    return $q.reject(errResponse);
                                }
                        );
            },

            delete: function (id) {
                console.log("deleging person " + id)
                return $http(Oauth.getRequest('/springboot-angular/sec/person/' + id, 'DELETE'))
                        .then(
                                function (response) {
                                    return response.data;
                                },
                                function (errResponse) {
                                    console.error('Error on delete person');
                                    return $q.reject(errResponse);
                                }
                        );

            }
        };

    }]);
