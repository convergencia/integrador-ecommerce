/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('CIntegrator').factory('LojaService', ['$resource', LojaService]);

function LojaService($resource){
    return $resource('api/loja/:id', {
        query: '@id'
    });
}

