/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('CIntegrator').factory('PedidoService', ['$resource', PedidoService]);

function PedidoService($resource) {
    return $resource('api/pedido/:id', {
        query: '@id'
    }, {
        reIntegrarPedidoEcommerce: {
            method: 'DELETE',
            cache: false,
            params: {
                query: '@id'
            },
            url: 'api/pedido/re-integrar/:id'
        }, 
        search: {
            method: 'GET',
            cache: false,
            url: 'api/pedido/search',
            isArray: true
        }
    });
}

