/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('CIntegrator').factory('PedidoRastreamentoService', ['$resource', PedidoRastreamentoService]);

function PedidoRastreamentoService($resource){
    return $resource('api/pedidorastreamento/:id', {
        query: '@id'
    });
}

