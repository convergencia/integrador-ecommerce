/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('CIntegrator').factory('PedidoPagamentoService', ['$resource', PedidoPagamentoService]);

function PedidoPagamentoService($resource){
    return $resource('api/pedidopagamento/:id', {
        query: '@id'
    });
}

