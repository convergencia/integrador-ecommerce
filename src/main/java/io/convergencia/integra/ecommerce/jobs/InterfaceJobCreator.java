/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.ecommerce.jobs;

import io.convergencia.integra.ecommerce.enuns.EInterfaces;
import io.convergencia.integra.ecommerce.model.InterfaceJob;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import io.convergencia.integra.ecommerce.repository.InterfaceJobDao;

/**
 *
 * @author wanderson
 */
@Component(value = "interfaceJobCreatorErp")
public class InterfaceJobCreator {

    @Autowired
    private InterfaceJobDao interfaceJobDao;

    @PostConstruct
    public void init() {

        for (EInterfaces eInterfaces : EInterfaces.values()) {

            if (interfaceJobDao.findByNome(eInterfaces.name()) == null) {
                InterfaceJob ij = new InterfaceJob();

                ij.setNome(eInterfaces.name());
                ij.setDescricao(eInterfaces.getDescriao());
                ij.setTipoIntegracao(eInterfaces.getTipoIntegracao());
                ij.setAtiva(false);

                interfaceJobDao.save(ij);
            }

        }

    }
}
