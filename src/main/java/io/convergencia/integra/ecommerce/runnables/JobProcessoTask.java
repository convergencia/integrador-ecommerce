/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.ecommerce.runnables;

import io.convergencia.integra.ecommerce.intefaces.IJobProcesso;

/**
 *
 * @author wanderson
 */
public class JobProcessoTask implements Runnable {

    private final IJobProcesso jobProcesso;

    public JobProcessoTask(IJobProcesso jobProcesso) {
        this.jobProcesso = jobProcesso;
    }

    @Override
    public void run() {

        boolean processando = jobProcesso.isProcessando();
        do {
            if (!processando) {
                jobProcesso.doProcessar();
            }

            try {
                Thread.sleep(1500L);
            } catch (InterruptedException e) {

            }
            processando = jobProcesso.isProcessando();
        } while (processando);
    }
}
