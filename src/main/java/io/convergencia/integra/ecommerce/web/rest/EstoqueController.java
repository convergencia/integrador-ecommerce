package io.convergencia.integra.ecommerce.web.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.convergencia.integra.ecommerce.model.Estoque;
import io.convergencia.integra.ecommerce.model.Produto;
import io.convergencia.integra.ecommerce.repository.EstoqueDao;
import io.convergencia.integra.ecommerce.specifications.EstoqueSpecification;

/**
 * @author Wanderson
 */
@RestController
@RequestMapping(value = "/api/estoque")
public class EstoqueController {

    @Autowired
    private EstoqueDao dao;

    @RequestMapping(method = RequestMethod.GET)
    public List<Estoque> person() {
        List<Estoque> target = new ArrayList<>();
        dao.findAll().forEach(target::add);
        return target;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public List<Estoque> prodBy(@RequestParam(value = "code", required = false) Integer code,
                                @RequestParam(value = "desc", required = false) String desc,
                                @RequestParam(value = "isPrecoZero", required = false) Boolean isPrecoZerado,
                                @RequestParam(value = "isEstoqueZero", required = false) Boolean isEstoqueZerado) {
        Specification<Estoque> searchPagamento = EstoqueSpecification.searchEstoue(code, desc, isPrecoZerado, isEstoqueZerado);
        List<Estoque> target = new ArrayList<>();
        dao.findAll(searchPagamento, new PageRequest(0, 100)).forEach(target::add);
        return target;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Estoque person(@PathVariable Long id) {
        Estoque findOne = dao.findOne(id);
        return findOne;
    }

    @RequestMapping(value = "/produto", method = RequestMethod.POST)
    public Estoque estoqueByProduto(@RequestBody Produto p) {
        Estoque findByProduto = dao.findByProduto(p);
        return findByProduto;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> save(@RequestBody Estoque person) {
        dao.save(person);
        return ResponseEntity.ok(null);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        dao.delete(id);
        return ResponseEntity.ok(null);
    }

}
