package io.convergencia.integra.ecommerce.web.rest;

import br.com.ambientinformatica.util.UtilVersao;
import java.util.Collections;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wanderson on 24/02/17.
 */
@RestController
@RequestMapping(value = "/application")
public class ApplicationController {

    @Autowired
    private Environment env;

    @RequestMapping(value = "/version", method = RequestMethod.GET)
    public Map<String, String> v() {

        return Collections.singletonMap("version", UtilVersao.getDetalhesVersaoJar("io.convergencia", "interface-ecommerce"));
    }

    @RequestMapping(value = "/profiles", method = RequestMethod.GET)
    public Map<String, String[]> profiles() {

        return Collections.singletonMap("profiles", env.getActiveProfiles());
    }
}
