/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.ecommerce.web.rest;

import io.convergencia.integra.ecommerce.model.ProdutoPrecificacao;
import io.convergencia.integra.ecommerce.repository.ProdutoPrecificacaoDao;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Wanderson
 */
@RestController
@RequestMapping(value = "/api/precificacao")
public class ProdutoPrecificacaoController {

    @Autowired
    private ProdutoPrecificacaoDao dao;

    @RequestMapping(method = RequestMethod.GET)
    public List<ProdutoPrecificacao> getAll() {
        List<ProdutoPrecificacao> target = new ArrayList<>();
        dao.findAll().forEach(target::add);
        return target;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ProdutoPrecificacao getById(@PathVariable Long id) {
        return dao.findOne(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> save(@RequestBody ProdutoPrecificacao o) {
        dao.save(o);
        return ResponseEntity.ok(null);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        dao.delete(id);
        return ResponseEntity.ok(null);
    }

}
