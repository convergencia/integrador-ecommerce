package io.convergencia.integra.ecommerce.web.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.convergencia.integra.ecommerce.enuns.EInterfaces;
import io.convergencia.integra.ecommerce.factories.ComponentsFactory;
import io.convergencia.integra.ecommerce.intefaces.IJobProcesso;
import io.convergencia.integra.ecommerce.model.InterfaceJob;
import io.convergencia.integra.ecommerce.repository.InterfaceJobDao;
import io.convergencia.integra.ecommerce.runnables.JobProcessoTask;

/**
 * @author wanderson
 */
@RestController
@RequestMapping(value = "/api/job")
public class JobController {
    @Autowired
    private InterfaceJobDao dao;

    @Autowired
    private ComponentsFactory componentsFactory;

    @RequestMapping(method = RequestMethod.GET)
    public List<InterfaceJob> person() {
        List<InterfaceJob> target = new ArrayList<>();
        dao.findAll().forEach(target::add);
        return target;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public InterfaceJob person(@PathVariable Long id) {
        return dao.findOne(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> save(@RequestBody InterfaceJob obj) {
        dao.save(obj);
        return ResponseEntity.ok(null);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        dao.delete(id);
        return ResponseEntity.ok(null);
    }

    @RequestMapping(value = "/manual", method = RequestMethod.GET)
    public Map<String, Object> processoManual(@RequestParam(value = "interface") EInterfaces job) {

        Map<String, Object> m = new HashMap<>();

        IJobProcesso jobProcesso = componentsFactory.getComponent(job.getTipoInterface());

        if (!jobProcesso.isProcessando()) {
            jobProcesso.initValoresParaProcesso();
            Thread thread = new Thread(new JobProcessoTask(jobProcesso));
            thread.setName(job.name());
            thread.start();
            m.put("status", String.format("Job %s está em processamento aguarde", job.name()));
        } else {
            m.put("status", String.format("Job %s está em processamento aguarde", job.name()));
        }

        m.put("ultimoJob", jobProcesso.getUltimoJob());

        return m;


    }
    
}
