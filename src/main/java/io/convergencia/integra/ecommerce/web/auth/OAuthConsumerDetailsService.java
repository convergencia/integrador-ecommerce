package io.convergencia.integra.ecommerce.web.auth;

import io.convergencia.integra.ecommerce.model.Usuario;
import io.convergencia.integra.ecommerce.repository.UsuarioDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth.common.OAuthException;
import org.springframework.security.oauth.common.signature.SharedConsumerSecretImpl;
import org.springframework.security.oauth.provider.BaseConsumerDetails;
import org.springframework.security.oauth.provider.ConsumerDetails;
import org.springframework.security.oauth.provider.ConsumerDetailsService;
import org.springframework.stereotype.Component;

@Component
public class OAuthConsumerDetailsService implements ConsumerDetailsService {

    final static Logger log = LoggerFactory.getLogger(OAuthConsumerDetailsService.class);
    
    @Autowired
    private UsuarioDao dao;

    @Override
    public ConsumerDetails loadConsumerByConsumerKey(String consumerKey) throws OAuthException {
        BaseConsumerDetails cd;
        // NOTE: really lookup the key and secret, for the sample here we just hardcoded
        Usuario u = dao.findOneByUsername(consumerKey);

        if (null != u) {
            cd = new BaseConsumerDetails();
            cd.setConsumerKey(consumerKey);
            cd.setSignatureSecret(new SharedConsumerSecretImpl(u.getPassword()));
            cd.setConsumerName(u.getNome());
            cd.setRequiredToObtainAuthenticatedToken(false); // no token required (0-legged)
            cd.getAuthorities().add(new SimpleGrantedAuthority("ROLE_OAUTH")); // add the ROLE_OAUTH (can add others as well)
        } else {
            throw new OAuthException(String.format("Usuário [%s] não encontrado", consumerKey));
        }

        return cd;
    }

}
