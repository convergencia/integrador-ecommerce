package io.convergencia.integra.ecommerce.factories;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Created by wanderson on 07/03/17.
 */
@Component
public class ComponentsFactory implements ApplicationContextAware{

    private ApplicationContext ac;
    
    public <T> T getComponent(Class<T> requiredType){
        return ac.getBean(requiredType);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
       this.ac = applicationContext;
    }

}
