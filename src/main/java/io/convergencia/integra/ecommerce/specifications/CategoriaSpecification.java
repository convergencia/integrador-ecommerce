package io.convergencia.integra.ecommerce.specifications;

import io.convergencia.integra.ecommerce.model.Categoria;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author wanderson
 */
public class CategoriaSpecification {

    public static Specification<Categoria> search(final Integer cdProd, final String descricao, final String tipo) {
        return (Root<Categoria> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (cdProd != null && cdProd > 0) {
                predicates.add(cb.equal(root.<Integer>get("codigo"), cdProd));
            } else {
                if (descricao != null && descricao.length() > 0) {
                    predicates.add(cb.like(root.<String>get("descricao"), descricao + "%"));
                }
            }

            switch (tipo) {
                case "departamento":
                    predicates.add(cb.equal(root.<Categoria>get("categoriaPai"), null));
                    break;
                    
                case "categoria" : break;
                case "subcategoria" : break;
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
