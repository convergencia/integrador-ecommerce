package io.convergencia.integra.ecommerce.specifications;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import io.convergencia.integra.ecommerce.model.Cliente;

/**
 *
 * @author wanderson
 */
public class ClienteIndexSpecification {

    public static Specification<Cliente> search(final Integer id, final String nome) {
        return (Root<Cliente> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (id != null && id > 0) {
                predicates.add(cb.equal(root.<Integer>get("id"), id));
            } else {
                if (nome != null && nome.length() > 0) {
                    predicates.add(cb.like(root.<String>get("nome"), nome + "%"));
                }
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
