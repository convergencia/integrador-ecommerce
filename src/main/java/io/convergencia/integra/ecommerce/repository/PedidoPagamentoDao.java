package io.convergencia.integra.ecommerce.repository;

import io.convergencia.integra.ecommerce.model.Pedido;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import io.convergencia.integra.ecommerce.model.PedidoPagamento;
import java.util.List;

@RepositoryRestResource(collectionResourceRel = "pedidopagamentos", path = "pedidopagamento")
public interface PedidoPagamentoDao extends PagingAndSortingRepository<PedidoPagamento, Long> {

    List<PedidoPagamento> findByPedido(Pedido p);
}
