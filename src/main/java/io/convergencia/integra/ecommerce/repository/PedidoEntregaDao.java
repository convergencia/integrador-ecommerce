package io.convergencia.integra.ecommerce.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import io.convergencia.integra.ecommerce.model.PedidoEntrega;

@RepositoryRestResource(collectionResourceRel = "pedido-entregas", path = "pedido-entrega")
public interface PedidoEntregaDao extends PagingAndSortingRepository<PedidoEntrega, Long>{

}
