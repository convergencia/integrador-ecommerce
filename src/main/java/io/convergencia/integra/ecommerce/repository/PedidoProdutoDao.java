package io.convergencia.integra.ecommerce.repository;

import io.convergencia.integra.ecommerce.model.Pedido;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import io.convergencia.integra.ecommerce.model.PedidoProduto;
import java.util.List;

@RepositoryRestResource(collectionResourceRel = "pedido-produtos", path = "Pedido-produto")
public interface PedidoProdutoDao extends PagingAndSortingRepository<PedidoProduto, Long>{

    List<PedidoProduto> findByPedido(Pedido p);
}
