/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.ecommerce.repository;

import io.convergencia.integra.ecommerce.enuns.EStatusVinculoErp;
import io.convergencia.integra.ecommerce.model.PedidoVincErp;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author wanderson
 */
@RepositoryRestResource(collectionResourceRel = "pedidoVincErp", path = "pedido-vinc-erp")
public interface PedidoVincErpDao extends PagingAndSortingRepository<PedidoVincErp, Long> {

    List<PedidoVincErp> findByStatusVinculoErp(EStatusVinculoErp statusVinculoErp);

}
