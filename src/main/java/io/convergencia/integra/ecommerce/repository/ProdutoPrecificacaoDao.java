/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.ecommerce.repository;

import io.convergencia.integra.ecommerce.model.ProdutoPrecificacao;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author wanderson
 */
@RepositoryRestResource(collectionResourceRel = "produtos-precificacoes", path = "produto-precificacao")
public interface ProdutoPrecificacaoDao extends PagingAndSortingRepository<ProdutoPrecificacao, Long> {

}
