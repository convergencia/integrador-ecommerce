package io.convergencia.integra.ecommerce.repository;

import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author wanderson
 */
@Component
public class ServerComponent {

    @Autowired
    private EntityManager entityManager;

    public Date getSystemDate() {
        Query query = entityManager.createNativeQuery("SELECT CURRENT_TIMESTAMP", Date.class);
        return (Date) query.getSingleResult();
    }
}
