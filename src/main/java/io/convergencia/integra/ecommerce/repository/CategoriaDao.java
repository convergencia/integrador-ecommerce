package io.convergencia.integra.ecommerce.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import io.convergencia.integra.ecommerce.model.Categoria;
import java.util.List;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

@RepositoryRestResource(collectionResourceRel = "categorias", path = "categoria")
public interface CategoriaDao extends PagingAndSortingRepository<Categoria, Long>, JpaSpecificationExecutor<Categoria> {

    List<Categoria> findByCategoriaPai(Categoria c);

    @Query(value = "Select c from Categoria c where c.categoriaPai is null")
    List<Categoria> departamentos();

    @Query(value = "Select c from Categoria c inner join c.categoriaPai p where p.categoriaPai is null ")
    List<Categoria> categorias();

    @Query(value = "Select c from Categoria c inner join c.categoriaPai p where not p.categoriaPai is null ")
    List<Categoria> subcategorias();
}
