package io.convergencia.integra.ecommerce.model;

import io.convergencia.integra.ecommerce.enuns.EStatusErpIntegrado;
import io.convergencia.integra.ecommerce.enuns.EStatusIntegracao;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author Wanderson
 */
@Entity
@Table(name = "INTEGRA_ECOMMERCE_PEDIDO")
public class PedidoIndex extends Entidade {

    private static final long serialVersionUID = 1L;

    @JoinColumn(name = "CLIENTE_ID", nullable = false)
    @ManyToOne(optional = false)
    @Fetch(FetchMode.JOIN)
    private Cliente cliente;

    @Column(name = "CODIGO_PEDIDO")
    private Long codigoPedido;

    @Column(name = "DATA_COMPRA")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataCompra;

    @Column(name = "DATA_")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataEntrega;

    @Column(name = "QUANTIDADE_ITENS")
    private Integer quantidadeItens;

    @Column(name = "VALOR_TOTAL_PRODUTOS")
    private BigDecimal valorTotalProdutos;

    @Column(name = "VALOR_TOTAL_DESCONTOS")
    private BigDecimal valorTotalDescontos;

    @Column(name = "VALOR_TOTAL_PEDIDO")
    private BigDecimal valorTotalPedido;

    @Column(name = "TIPO_FRETE")
    private String tipoFrete;

    @Column(name = "VALOR_FRETE")
    private BigDecimal valorFrete;

    @Column(name = "STATUS_ERP_INTEGRADO")
    @Enumerated
    private EStatusErpIntegrado statusErpIntegrado = EStatusErpIntegrado.AGUARDANDO;

    @Column(name = "STATUS_INTEGRACAO")
    @Enumerated
    private EStatusIntegracao statusIntegracao = EStatusIntegracao.NAO_INTEGRADO;

    @Column(name = "NOTIFICA_ECOMMERCE")
    private Boolean notificaEcommerce;

    @Column(name = "STATUS_ECOMMERCE")
    private String statusEcommerce;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Long getCodigoPedido() {
        return codigoPedido;
    }

    public void setCodigoPedido(Long codigoPedido) {
        this.codigoPedido = codigoPedido;
    }

    public Date getDataCompra() {
        return dataCompra;
    }

    public void setDataCompra(Date dataCompra) {
        this.dataCompra = dataCompra;
    }

    public Date getDataEntrega() {
        return dataEntrega;
    }

    public void setDataEntrega(Date dataEntrega) {
        this.dataEntrega = dataEntrega;
    }

    public Integer getQuantidadeItens() {
        return quantidadeItens;
    }

    public void setQuantidadeItens(Integer quantidadeItens) {
        this.quantidadeItens = quantidadeItens;
    }

    public BigDecimal getValorTotalProdutos() {
        return valorTotalProdutos;
    }

    public void setValorTotalProdutos(BigDecimal valorTotalProdutos) {
        this.valorTotalProdutos = valorTotalProdutos;
    }

    public BigDecimal getValorTotalDescontos() {
        return valorTotalDescontos;
    }

    public void setValorTotalDescontos(BigDecimal valorTotalDescontos) {
        this.valorTotalDescontos = valorTotalDescontos;
    }

    public BigDecimal getValorTotalPedido() {
        return valorTotalPedido;
    }

    public void setValorTotalPedido(BigDecimal valorTotalPedido) {
        this.valorTotalPedido = valorTotalPedido;
    }

    public String getTipoFrete() {
        return tipoFrete;
    }

    public void setTipoFrete(String tipoFrete) {
        this.tipoFrete = tipoFrete;
    }

    public BigDecimal getValorFrete() {
        return valorFrete;
    }

    public void setValorFrete(BigDecimal valorFrete) {
        this.valorFrete = valorFrete;
    }

    public EStatusErpIntegrado getStatusErpIntegrado() {
        return statusErpIntegrado;
    }

    public void setStatusErpIntegrado(EStatusErpIntegrado statusErpIntegrado) {
        this.statusErpIntegrado = statusErpIntegrado;
    }

    public EStatusIntegracao getStatusIntegracao() {
        return statusIntegracao;
    }

    public void setStatusIntegracao(EStatusIntegracao statusIntegracao) {
        this.statusIntegracao = statusIntegracao;
    }

    public Boolean getNotificaEcommerce() {
        return notificaEcommerce;
    }

    public void setNotificaEcommerce(Boolean notificaEcommerce) {
        this.notificaEcommerce = notificaEcommerce;
    }

    public String getStatusEcommerce() {
        return statusEcommerce;
    }

    public void setStatusEcommerce(String statusEcommerce) {
        this.statusEcommerce = statusEcommerce;
    }

    
}
