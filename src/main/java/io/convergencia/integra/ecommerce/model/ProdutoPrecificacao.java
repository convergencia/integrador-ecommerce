package io.convergencia.integra.ecommerce.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author wanderson
 */
@Entity
@Table(name = "INTEGRA_ECOMMERCE_PRECIFICACAO")
@DynamicUpdate
public class ProdutoPrecificacao extends Entidade {

    private static final long serialVersionUID = 1L;

    private BigDecimal valor;
    private BigDecimal valorOferta;
    

    private Estoque estoque;

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public BigDecimal getValorOferta() {
        return valorOferta;
    }

    public void setValorOferta(BigDecimal valorOferta) {
        this.valorOferta = valorOferta;
    }

    public Estoque getEstoque() {
        return estoque;
    }

    public void setEstoque(Estoque estoque) {
        this.estoque = estoque;
    }

}
