/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.ecommerce.creators;

import io.convergencia.integra.ecommerce.enuns.EPerfil;
import io.convergencia.integra.ecommerce.model.Usuario;
import io.convergencia.integra.ecommerce.repository.UsuarioDao;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author wanderson
 */
@Component
public class UsuarioCreator {

    @Autowired
    private UsuarioDao ud;

    @PostConstruct
    public void init() {

        Usuario u = ud.findOneByUsername("admin");
        if (null == u) {
            u = new Usuario();
            u.setEmail("");
            u.setNome("Administrador");
            u.setPerfil(EPerfil.ADMIN);
            u.setUsername("admin");
            u.setPassword("123456");

            ud.save(u);
        }
    }

}

